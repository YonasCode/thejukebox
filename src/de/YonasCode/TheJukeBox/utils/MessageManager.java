package de.YonasCode.TheJukeBox.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class MessageManager {

	private static MessageManager instance = new MessageManager();
	public String prefix = ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "[" + ChatColor.RESET + ChatColor.GOLD + "TheJukeBox" + ChatColor.DARK_GRAY + ChatColor.BOLD + "] " + ChatColor.RESET;
	
	public enum MessageType {
		
		INFO(ChatColor.GRAY),
		GOOD(ChatColor.GOLD),
		BAD(ChatColor.RED),
		NORMAL(ChatColor.RESET);
		
		private ChatColor color;
		
		MessageType(ChatColor color) {
			this.color = color;
		}
		
		public ChatColor getColor() {
			return color;
		}
	}
	
	public enum LogType {
		
		INFO(ChatColor.YELLOW),
		GOOD(ChatColor.GREEN),
		BAD(ChatColor.RED),
		SEVERE(ChatColor.DARK_RED);
		
		private ChatColor color;
		
		LogType(ChatColor color) {
			this.color = color;
		}
		
		public ChatColor getColor() {
			return color;
		}
		 
	}
	
	private MessageManager() {}
	
	public static MessageManager getInstance() {
		return instance;
	}
	
	public void message(CommandSender sender, MessageType type, String... messages) {
		for(String m : messages) {
			sender.sendMessage(prefix + type.getColor() + m);
		}
	}
	
	public void log(LogType type, String... messages) {
		for(String m : messages) {
			Bukkit.getConsoleSender().sendMessage(prefix + type.getColor() + m);
		}
	}

	public void log(LogType type, StackTraceElement[] stackTrace) {
		for(StackTraceElement st : stackTrace) {
			Bukkit.getConsoleSender().sendMessage(prefix + type.getColor() + st);
		}
	}
	
}
