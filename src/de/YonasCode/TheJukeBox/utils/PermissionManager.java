package de.YonasCode.TheJukeBox.utils;

import org.bukkit.entity.Player;

import de.YonasCode.TheJukeBox.utils.MessageManager.MessageType;

public class PermissionManager {

	private static PermissionManager instance = new PermissionManager();
	
	public static PermissionManager getInstance() {
		return instance;
	}
	
	public enum Permission {
		
		NEXT("thejukebox.next"),
		PLAY("thejukebox.play"),
		REFRESH("thejukebox.refresh"),
		LIST("thejukebox.list"),
		MUSIC("thejukebox.music");
		
		private String text;
		
		Permission(String text) {
			this.text = text;
		}
	
		public String getPermission() {
			return text;
		}	
		
	}
	
	public boolean hasPermission(Player p, Permission permission) {
		if(p.hasPermission(permission.getPermission())) {
			return true;
		} else {
			MessageManager.getInstance().message(p, MessageType.BAD, "You don't have permission for this.");
			return false;
		}
	}
	
}
