package de.YonasCode.TheJukeBox;

import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import de.YonasCode.TheJukeBox.Music.MusicManager;

public class MusicListener implements Listener {

	@EventHandler
	public void onKick(PlayerKickEvent e) {
		if(MusicManager.getInstance().getCurrently().contains(e.getPlayer())) {
			MusicManager.getInstance().getCurrently().remove(e.getPlayer());
		}
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		if(MusicManager.getInstance().getCurrently().contains(e.getPlayer())) {
			MusicManager.getInstance().getCurrently().remove(e.getPlayer());
		}
	}
	
}
