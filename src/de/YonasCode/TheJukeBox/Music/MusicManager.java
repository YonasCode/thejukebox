package de.YonasCode.TheJukeBox.Music;

import java.io.File;
import java.util.ArrayList;

import de.YonasCode.TheJukeBox.TheJukeBox;

public class MusicManager {

	private static MusicManager instance = new MusicManager();
	private ArrayList<Music> musics = new ArrayList<Music>();
	private Music currently = null;
	
	public static MusicManager getInstance() {
		return instance;
	}
	
	public void load() {
		if(currently != null) MusicManager.getInstance().getCurrently().stop();
		musics.clear();
		//add part
		int i = 0;
		for(File file : TheJukeBox.getInstance().getDataFolder().listFiles()) {
			if(!(file.isDirectory()) && file.getName().toLowerCase().endsWith(".nbs")) {
				musics.add(new Music(i, file));
				i++;
			}
		}
		
		if(!(musics.isEmpty())) {
			musics.get(0).start();	
		}
	}
	
	protected void set(Music currently) {
		this.currently = currently;
	}
	
	public Music getCurrently() {
		return currently;
	}
	
	public Music getNext() {
		int i = currently.getID()+1;
		for(Music m : musics) {
			if(m.getID() == i) return m; 
		}
		return null;
	}
	
	public Music getByID(int id) {
		return musics.get(id);
	}
	
	public ArrayList<Music> getList() {
		return musics;
	}
	
	
}
