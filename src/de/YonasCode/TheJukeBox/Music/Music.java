package de.YonasCode.TheJukeBox.Music;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import com.xxmicloxx.NoteBlockAPI.NBSDecoder;
import com.xxmicloxx.NoteBlockAPI.RadioSongPlayer;
import com.xxmicloxx.NoteBlockAPI.Song;

import de.YonasCode.TheJukeBox.TheJukeBox;
import de.YonasCode.TheJukeBox.utils.MessageManager;
import de.YonasCode.TheJukeBox.utils.MessageManager.MessageType;

public class Music {

	private int id;
	private String author, title;
	private File music;
	
	//songs
	private Song song;
	private RadioSongPlayer radio;
	
	//settings
	private BukkitTask task;
	
	public Music(int id, File music) {
		this.id = id;
		this.music = music;
		
		this.song = NBSDecoder.parse(this.music);
		this.author = this.song.getAuthor();
		this.title = this.song.getTitle();
	}
	
	public int getID() {
		return id;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public String getTitle() {
		return title;
	}
	
	public long getTime() {
		return ((this.song.getLength() / this.song.getSpeed()) * 20) - ((this.song.getDelay() * 20));
	}
	
	public long getPosition() {
		return (getTime() - ((this.radio.getTick() / this.song.getSpeed()) * 20) - (this.song.getDelay() * 20));
	}
	
	public long timeOver() {
		return (getTime() - getPosition());
	}
	
	public String getTimeAsText() {
		int i = ((int)getTime() / 20);
		long minutes = TimeUnit.SECONDS.toMinutes(i) - (TimeUnit.SECONDS.toHours(i)* 60);
		long seconds = TimeUnit.SECONDS.toSeconds(i) - (TimeUnit.SECONDS.toMinutes(i) *60);
		return String.format("%d:%02d", minutes, (seconds + this.song.getDelay()));
	}
	
	public String getPositionAsText() {
		int i = (int)getPosition() / 20;
		long minutes = TimeUnit.SECONDS.toMinutes(i) - (TimeUnit.SECONDS.toHours(i)* 60);
		long seconds = TimeUnit.SECONDS.toSeconds(i) - (TimeUnit.SECONDS.toMinutes(i) *60);
		return String.format("%d:%02d", minutes, seconds);
	}
	
	public String timeOverAsText() {
		int i = (int)timeOver() / 20;
		long minutes = TimeUnit.SECONDS.toMinutes(i) - (TimeUnit.SECONDS.toHours(i)* 60);
		long seconds = TimeUnit.SECONDS.toSeconds(i) - (TimeUnit.SECONDS.toMinutes(i) *60);
		return String.format("%d:%02d", minutes, seconds);
	}
	
	public File getMusic() {
		return music;
	}
	
	public List<String> getPlayers() {
		return this.radio.getPlayerList();
	}
	
	public void start() {
		//define
		this.song = NBSDecoder.parse(music);
		this.radio = new RadioSongPlayer(song);
		
		this.author = this.song.getAuthor();
		this.title = this.song.getTitle();
		
		//work
		this.radio.setAutoDestroy(false);
		this.radio.setPlaying(true);
		MusicManager.getInstance().set(this);
		task = Bukkit.getServer().getScheduler().runTaskLater(TheJukeBox.getInstance(), new Runnable() {

			@Override
			public void run() {
				stop();
				List<String> players = MusicManager.getInstance().getCurrently().getPlayers();
				Music m = MusicManager.getInstance().getNext();
				if(m == null) {
					m = MusicManager.getInstance().getByID(0);
					m.start();
					for(String s : players) {
						MusicManager.getInstance().getByID(0).add(Bukkit.getPlayer(s));
						MessageManager.getInstance().message(Bukkit.getPlayer(s), MessageType.NORMAL, ChatColor.GOLD + "Du h�rst nun das Musikst�ck " + ChatColor.GREEN + m.getTitle() + ChatColor.GOLD + " von " + ChatColor.GREEN + m.getAuthor() + ChatColor.GOLD + ".");
					}
					return;
				}
				
				//new
				m.start();
				for(String s : players) {
					m.add(Bukkit.getPlayer(s));
					MessageManager.getInstance().message(Bukkit.getPlayer(s), MessageType.NORMAL, ChatColor.GOLD + "Du h�rst nun das Musikst�ck " + ChatColor.GREEN + m.getTitle() + ChatColor.GOLD + " von " + ChatColor.GREEN + m.getAuthor() + ChatColor.GOLD + ".");
				}
			}
			
		}, this.getTime());
	}
	
	public void stop() {
		task.cancel();
		this.radio.setPlaying(false);
		this.radio.destroy();
	}
	
	public void add(Player p) {
		this.radio.addPlayer(p);
	}
	
	public void addAll(List<String> player) {
		for(String s : player) {
			this.radio.addPlayer(Bukkit.getPlayer(s));
		}
	}
	
	public void remove(Player p) {
		this.radio.removePlayer(p);
	}
	
	public boolean contains(Player p) {
		return this.radio.getPlayerList().contains(p.getName());
	}
	
}
