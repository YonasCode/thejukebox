package de.YonasCode.TheJukeBox;

import java.io.File;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import de.YonasCode.TheJukeBox.Music.Music;
import de.YonasCode.TheJukeBox.Music.MusicManager;
import de.YonasCode.TheJukeBox.utils.MessageManager;
import de.YonasCode.TheJukeBox.utils.PermissionManager;
import de.YonasCode.TheJukeBox.utils.MessageManager.MessageType;
import de.YonasCode.TheJukeBox.utils.PermissionManager.Permission;

public class TheJukeBox extends JavaPlugin {

	private static TheJukeBox instance;
	
	@Override
	public void onEnable() {
		instance = this;
		if(!(this.getDataFolder().exists())) this.getDataFolder().mkdirs();
		Bukkit.getServer().getPluginManager().registerEvents(new MusicListener(), instance);
		MusicManager.getInstance().load();
	}
	
	@Override
	public void onDisable() {
		if(MusicManager.getInstance().getCurrently() != null)
		MusicManager.getInstance().getCurrently().stop();
	}
	
	public static TheJukeBox getInstance() {
		return instance;
	}
	
	public File getFolder() {
		return this.getDataFolder();
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(!(sender instanceof Player)) return true;
		
		Player p = (Player)sender;
		
		if(cmd.getName().equalsIgnoreCase("music")) {
			
			if(args.length >= 1) {
				if(args[0].equalsIgnoreCase("list")) {
					
					if(!(PermissionManager.getInstance().hasPermission(p, Permission.LIST))) return true;
					
					if(MusicManager.getInstance().getList().size() == 0) {
						MessageManager.getInstance().message(p, MessageType.BAD, "Aktuell sind keine Title verfügbar.");
						return true;
					}
					
					MessageManager.getInstance().message(p, MessageType.NORMAL, ChatColor.GOLD + "" + ChatColor.BOLD + "|||||||||||||||||||||||||||||||||||||||||||||||||||||");
					for(Music m : MusicManager.getInstance().getList()) {
						if(MusicManager.getInstance().getCurrently().getID() != m.getID())
							MessageManager.getInstance().message(p, MessageType.NORMAL, ChatColor.GOLD + "" + (m.getID()+1) + " - " + ChatColor.GREEN + m.getTitle() + ChatColor.GOLD + " von " + ChatColor.GREEN + m.getAuthor() + ChatColor.GOLD + " (" + ChatColor.GRAY + m.getTimeAsText() + ChatColor.GOLD + ")");
						else
							MessageManager.getInstance().message(p, MessageType.NORMAL, ChatColor.GOLD + "" + (m.getID()+1) + " - " + ChatColor.GREEN + m.getTitle() + ChatColor.GOLD + " von " + ChatColor.GREEN + m.getAuthor() + ChatColor.GOLD + " (" + ChatColor.GRAY + m.timeOverAsText() + "-" + m.getTimeAsText() + ChatColor.GOLD + ")" + ChatColor.RED + "" + ChatColor.BOLD + " ♪Play♪");
					}
					MessageManager.getInstance().message(p, MessageType.NORMAL, ChatColor.GOLD + "" + ChatColor.BOLD + "|||||||||||||||||||||||||||||||||||||||||||||||||||||");
					return true;
				}
				
				
				if(args[0].equalsIgnoreCase("refresh")) {
					if(!(PermissionManager.getInstance().hasPermission(p, Permission.REFRESH))) {
						MusicManager.getInstance().load();
						MessageManager.getInstance().message(p, MessageType.NORMAL, ChatColor.GOLD + "Die Musikliste wurde aktualisiert, es sind nun " + ChatColor.GREEN + MusicManager.getInstance().getList().size() + ChatColor.GOLD + " Title verfügbar.");
						return true;
					}
					return true;
				}
				
				if(args[0].equalsIgnoreCase("next")) {
					
					if(MusicManager.getInstance().getList().size() == 0) {
						MessageManager.getInstance().message(p, MessageType.BAD, "Aktuell sind keine Title verfügbar.");
						return true;
					}
					
					if(!(PermissionManager.getInstance().hasPermission(p, Permission.NEXT))) {
						List<String> players = MusicManager.getInstance().getCurrently().getPlayers();
						MusicManager.getInstance().getCurrently().stop();
						Music m = MusicManager.getInstance().getNext();
						if(m == null) m = MusicManager.getInstance().getByID(0);
						m.start();
						m.addAll(players);
						for(String s : players) {
							MessageManager.getInstance().message(Bukkit.getPlayer(s), MessageType.NORMAL, ChatColor.GOLD + "Du hörst nun das Musikstück " + ChatColor.GREEN + m.getTitle() + ChatColor.GOLD + " von " + ChatColor.GREEN + m.getAuthor() + ChatColor.GOLD + ".");
						}
					}
				}
				
				if(args[0].equalsIgnoreCase("play")) {
					
					if(MusicManager.getInstance().getList().size() == 0) {
						MessageManager.getInstance().message(p, MessageType.BAD, "Aktuell sind keine Title verfügbar.");
						return true;
					}
					
					//permission check
					if(!(PermissionManager.getInstance().hasPermission(p, Permission.PLAY))) return true;
					
					if(args.length >= 2) {
						Music m = null;
						try {
						 m = MusicManager.getInstance().getByID(Integer.valueOf(args[1])-1);
						}catch(Exception e) {};
						if(m == null) {
							MessageManager.getInstance().message(p, MessageType.BAD, "Dieser Song ist uns nicht bekannt, sorry.");
							return true;
						}
						
						List<String> players = MusicManager.getInstance().getCurrently().getPlayers();
						MusicManager.getInstance().getCurrently().stop();
						m.start();
						m.addAll(players);
						for(String s : players) {
							MessageManager.getInstance().message(Bukkit.getPlayer(s), MessageType.NORMAL, ChatColor.GOLD + "Du hörst nun das Musikstück " + ChatColor.GREEN + m.getTitle() + ChatColor.GOLD + " von " + ChatColor.GREEN + m.getAuthor() + ChatColor.GOLD + ".");
						}
						MessageManager.getInstance().message(p, MessageType.GOOD, "Der Song wurde erfolgreich geändert.");
						return true;
					}
					return true;
				}
				
				return true;
			}
			
			//permission check
			if(!(PermissionManager.getInstance().hasPermission(p, Permission.MUSIC))) return true;
			
			if(MusicManager.getInstance().getList().size() == 0) {
				MessageManager.getInstance().message(p, MessageType.BAD, "Aktuell sind keine Title verfügbar.");
				return true;
			}
			
			Music m = MusicManager.getInstance().getCurrently();
			if(m == null) return true;
			
			if(m.contains(p)) {
				m.remove(p);
				MessageManager.getInstance().message(p, MessageType.NORMAL, ChatColor.GOLD + "Du hast aufgehört das Musikstück " + ChatColor.GREEN + m.getTitle() + ChatColor.GOLD + " zuhören.");
				return true;
			} 
			
			m.add(p);
			MessageManager.getInstance().message(p, MessageType.NORMAL, ChatColor.GOLD + "Du hörst nun das Musikstück " + ChatColor.GREEN + m.getTitle() + ChatColor.GOLD + " von " + ChatColor.GREEN + m.getAuthor() + ChatColor.GOLD + ".");
			
			return true;
		}
		
		return true;
	}
	
}
